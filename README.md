# Aether

Releases of the new custom client for Disney's Toontown Online (2013).

## Dependencies
* [Windows Client Panda3D](https://download.sunrise.games/panda3d/Panda3D-1.11.0-x64-aether.7z)
* [macOS Client Panda3D](https://download.sunrise.games/panda3d/Panda3D-1.11.0-x64-aether.dmg)
* [Launcher Panda3D](https://rocketprogrammer.me/binaries/Panda3D-1.11.0-py3.8-x64.exe)

### Getting Started
1.) Invoke this using your launcher Panda3D.

C:/Panda3D-1.11.0-x64-py3.8/python/python.exe -m pip install -r requirements.txt